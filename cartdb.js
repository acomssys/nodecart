// simple DB mock

const cartdb = {

  catalogue: [{ "name": "Apple", "price": 0.20},
              {"name": "Oranges", "price": 0.30},
              {"name": "Garlic", "price": 0.15},
              {"name": "Papaya", "price": 0.50, "discountComplett": 3, "complettPrice": 1.00},
      ],
  cart: [],

  // add new items
  add:
    function(newItem){

      newItem.amount = parseInt(newItem.amount, '10');
      if(isNaN(newItem.amount )) {
        console.log('Wrong amount!')
        return;
      }

      // TODO each item can be multiple times ??
      var item = this.cart.filter(function(value){ return value.name==newItem.name;})
      if(item.length > 0) {
          item[0].amount += newItem.amount;
          item[0].price = this.getprice(item[0]);
        }
        else {
          newItem.price = this.getprice(newItem);
          this.cart.push(newItem);
        }

      //console.log(this.cart);
    },

  clear:
    function(){
      this.cart = [];
    },

  // get all items
  getcartItems:
    function(){
      return this.cart;
    },

  // calculate item price and discount
  getprice:
    function(item){

      let itemList = this.catalogue.filter(function(value){ return value.name==item.name;})
      if(itemList.length > 0)
        {
          let catalItem = itemList[0];
          let discount = 0;
          let price = catalItem.price * item.amount;
          //console.log('Price: '+ price, catalItem.price, item.amount);
          if(catalItem.discountComplett) {

            var numCompletts = Math.trunc(item.amount/catalItem.discountComplett);
            let actualPrice = numCompletts * catalItem.complettPrice + (item.amount % catalItem.discountComplett) * catalItem.price;

            //console.log('Completts: '+ numCompletts  );
            discount = actualPrice - price;
            price = actualPrice;
          }

          price = Math.round(price * 100) / 100;
          discount = Math.round(discount * 100) / 100;

          return {
            total: price,
            discount: discount
          };
        }
      else return null;
    },

    // get total amount
    gettotal:
      function(){
        let total = 0;
        this.cart.map((item) => {
          total += item.price.total;
        });
        return Math.round(total * 100) / 100
      }

}

module.exports = cartdb;
