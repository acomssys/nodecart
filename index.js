const express = require('express');
const bodyParser= require('body-parser');
const app = express();

const cartdb = require('./cartdb.js');

app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');

// main rendering page
app.get('/', (req, res) => {
  // TODO async
  let cart = cartdb.getcartItems();
  let total = cartdb.gettotal();
  res.render('index.ejs', {cart: cart, total: total});

})

// just in case if someone will need JSON
app.get('/receipt', (req, res) => {
  // TODO async
  let cart = cartdb.getcartItems();
  res.json(cart);
})

// add new item
app.post('/cart', (req, res) => {
  cartdb.add(req.body);
  let total = cartdb.gettotal();
  res.redirect('/');
})

// clear cart items
app.post('/clear', (req, res) => {
  cartdb.clear();
  res.redirect('/');
})

// in version 239.044 - here will be PayPal interface :)
app.post('/checkout', (req, res) => {
  // TODO payment processing
})

app.listen(3000, () => {
  console.log('listening on 3000')
})
